EXE = scpipe

ARCHFLAGS = -arch x86_64

CFLAGS = -Os

SCPIPE_OBJS  = scpipe.o SCCore.o

.PHONY: all clean

all: $(EXE)

scpipe: $(SCPIPE_OBJS)
	$(CXX) $(ARCHFLAGS) $^ -o $@ -Xlinker -sectcreate -Xlinker __TEXT -Xlinker __info_plist -Xlinker Info.plist -framework Foundation
	codesign -f -s "Developer ID Application: Christopher Snowhill" --entitlements scpipe.entitlements -o runtime $@

scpipe.o: scpipe.mm SCCore.h SCCore.cpp
	$(CXX) -c $(ARCHFLAGS) $(CFLAGS) scpipe.mm -o $@

SCCore.o: SCCore.cpp SCCore.h
	$(CXX) -c $(ARCHFLAGS) $(CFLAGS) SCCore.cpp -o $@

clean:
	rm -rf $(SCPIPE_OBJS) $(EXE) > /dev/null
